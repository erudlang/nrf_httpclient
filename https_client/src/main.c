/*
 * Copyright (c) 2020 Nordic Semiconductor ASA
 *
 * SPDX-License-Identifier: LicenseRef-BSD-5-Clause-Nordic
 */
#include <logging/log.h>
LOG_MODULE_REGISTER(net_http_client_sample, LOG_LEVEL_DBG);
#include <string.h>
#include <device.h>
#include <zephyr.h>
#include <stdlib.h>
#include <net/socket.h>
#include <modem/bsdlib.h>
#include <net/tls_credentials.h>
#include <modem/lte_lc.h>
#include <modem/at_cmd.h>
#include <modem/at_notif.h>
#include <modem/modem_key_mgmt.h>
#include <net/http_client.h>
#include <nrfx_gpiote.h>

#include <nrfx_pdm.h>

#define HTTPS_PORT 443

#define HTTP_HEAD                                                              \
	"HEAD / HTTP/1.1\r\n"                                                  \
	"Host: www.google.com:443\r\n"                                         \
	"Connection: close\r\n\r\n"

#define HTTP_HEAD_LEN (sizeof(HTTP_HEAD) - 1)

#define HTTP_HDR_END "\r\n\r\n"

#define RECV_BUF_SIZE 2048
#define TLS_SEC_TAG 42

#define MAX_RECV_BUF_LEN 812
#define PDM_BUFFER_SIZE 4096

#define  SCK_PIN    10 
#define  SDIN_PIN   11
#define  LRCK_PIN   12
#define  MCK_PIN    30
#define  SDOUT_PIN  31 

int counter;
static uint32_t rx_buffer[I2S_BUFFER_SIZE]={0};
static uint32_t tx_buffer[I2S_BUFFER_SIZE]={0};
static nrfx_i2s_buffers_t buffer;   

static char recv_buf_ipv4[MAX_RECV_BUF_LEN];
static int16_t pdm_buffer[PDM_BUFFER_SIZE];

static const char send_buf[] = HTTP_HEAD;
static char recv_buf[RECV_BUF_SIZE];


/* Certificate for `google.com` */
static const char cert[] = {
	#include "../cert/output.pem"
};

BUILD_ASSERT(sizeof(cert) < KB(4), "Certificate too large");


/* Initialize AT communications */
int at_comms_init(void)
{
	int err;

	err = at_cmd_init();
	if (err) {
		printk("Failed to initialize AT commands, err %d\n", err);
		return err;
	}

	err = at_notif_init();
	if (err) {
		printk("Failed to initialize AT notifications, err %d\n", err);
		return err;
	}

	return 0;
}

/* Provision certificate to modem */
int cert_provision(void)
{
	int err;
	bool exists;
	uint8_t unused;

	err = modem_key_mgmt_exists(TLS_SEC_TAG,
				    MODEM_KEY_MGMT_CRED_TYPE_CA_CHAIN,
				    &exists, &unused);
	if (err) {
		printk("Failed to check for certificates err %d\n", err);
		return err;
	}

	if (exists) {
		/* For the sake of simplicity we delete what is provisioned
		 * with our security tag and reprovision our certificate.
		 */
		err = modem_key_mgmt_delete(TLS_SEC_TAG,
					    MODEM_KEY_MGMT_CRED_TYPE_CA_CHAIN);
		if (err) {
			printk("Failed to delete existing certificate, err %d\n",
			       err);
		}
	}

	printk("Provisioning certificate\n");

	/*  Provision certificate to the modem */
	err = modem_key_mgmt_write(TLS_SEC_TAG,
				   MODEM_KEY_MGMT_CRED_TYPE_CA_CHAIN,
				   cert, sizeof(cert) - 1);
	if (err) {
		printk("Failed to provision certificate, err %d\n", err);
		return err;
	}

	return 0;
}

/* Setup TLS options on a given socket */
int tls_setup(int fd)
{
	int err;
	int verify;

	/* Security tag that we have provisioned the certificate with */
	const sec_tag_t tls_sec_tag[] = {
		TLS_SEC_TAG,
	};

	/* Set up TLS peer verification */
	enum {
		NONE = 0,
		OPTIONAL = 1,
		REQUIRED = 2,
	};

	verify = REQUIRED;

	err = setsockopt(fd, SOL_TLS, TLS_PEER_VERIFY, &verify, sizeof(verify));
	if (err) {
		printk("Failed to setup peer verification, err %d\n", errno);
		return err;
	}

	/* Associate the socket with the security tag
	 * we have provisioned the certificate with.
	 */
	err = setsockopt(fd, SOL_TLS, TLS_SEC_TAG_LIST, tls_sec_tag,
			 sizeof(tls_sec_tag));
	if (err) {
		printk("Failed to setup TLS sec tag, err %d\n", errno);
		return err;
	}

	return 0;
}
static int payload_cb(int sock, struct http_request *req, void *user_data)
{
	const char *content[] = {
		"foobar",
		"chunked",
		"last"
	};
	char tmp[64];
	int i, pos = 0;

	for (i = 0; i < ARRAY_SIZE(content); i++) {
		pos += snprintk(tmp + pos, sizeof(tmp) - pos,
				"%x\r\n%s\r\n",
				(unsigned int)strlen(content[i]),
				content[i]);
	}

	pos += snprintk(tmp + pos, sizeof(tmp) - pos, "0\r\n\r\n");

	(void)send(sock, tmp, pos, 0);

	return pos;
}

static void response_cb(struct http_response *rsp,
			enum http_final_call final_data,
			void *user_data)
{
	if (final_data == HTTP_DATA_MORE) {
		printk("Partial data received (%zd bytes)", rsp->data_len);
	} else if (final_data == HTTP_DATA_FINAL) {
		printk("All the data received (%zd bytes)", rsp->data_len);
	}

	LOG_INF("Response to %s", (const char *)user_data);
	LOG_INF("Response status %s", rsp->http_status);
}

int audio_init(){


}

static void i2s_event_handler(nrfx_pdm_evt_t const const* evt)
{
  int16_t *buffer_released = evt->buffer_released;
  if(evt->buffer_requested)
  {
    if(buffer_released)
    {
      printk("Out: %2x %2x\r\n", (uint16_t)pdm_buffer[0],(uint16_t)pdm_buffer[1]);
      
      nrfx_pdm_buffer_set(buffer_released,PDM_BUFFER_SIZE);
    }
    else
    {
      nrfx_pdm_buffer_set(&pdm_buffer, PDM_BUFFER_SIZE);
    }
   }
}

ISR_DIRECT_DECLARE(i2s_isr_handler)
{
	nrfx_i2s_irq_handler();
	ISR_DIRECT_PM(); /* PM done after servicing interrupt for best latency
			  */

	return 1; /* We should check if scheduling decision should be made */
}
   







void main(void)
{
	int err;
	int fd;
	char *p;
	int bytes;
	size_t off;
        bool enabled = true;
        nrfx_err_t error;

        IRQ_DIRECT_CONNECT(I2S_IRQn, 0, i2s_isr_handler, 0);


        uint32_t nrf_gpoite_literal_address = NRF_PDM;


        int32_t timeout = 3 * MSEC_PER_SEC;
	struct addrinfo *res;
	struct addrinfo hints = {
		.ai_family = AF_INET,
		.ai_socktype = SOCK_STREAM,
	};


        

        nrfx_pdm_config_t const p_config = NRFX_I2S_DEFAULT_CONFIG(SCK_PIN, LRCK_PIN, MCK_PIN, SDOUT_PIN, SDIN_PIN;
        error = nrfx_i_init(&p_config, pdm_event_handler);

        if(error != NRFX_SUCCESS){
          printk("Failed to initialize pdm");
          return;
        }

        error = nrfx_pdm_start();
        if(error != NRFX_SUCCESS){
          printk("Failed to start pdm");
        }

        enabled = nrfx_pdm_enable_check();


	printk("HTTPS client sample started\n\r");

	err = bsdlib_init();
	if (err) {
		printk("Failed to initialize bsdlib!");
		return;
	}

	/* Initialize AT comms in order to provision the certificate */
	err = at_comms_init();
	if (err) {
		return;
	}

	/* Provision certificates before connecting to the LTE network */
	err = cert_provision();
	if (err) {
		return;
	}

	printk("Waiting for network.. ");
	err = lte_lc_init_and_connect();
	if (err) {
		printk("Failed to connect to the LTE network, err %d\n", err);
		return;
	}
	printk("OK\n");

	err = getaddrinfo("babysense.io", NULL, &hints, &res);
	if (err) {
		printk("getaddrinfo() failed, err %d\n", errno);
		return;
	}

        char hostString[12];
        sprintf(hostString,"%d.%d.%d.%d",res->ai_addr->data[2],res->ai_addr->data[3],res->ai_addr->data[4],res->ai_addr->data[5]);

	((struct sockaddr_in *)res->ai_addr)->sin_port = htons(HTTPS_PORT);

	fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TLS_1_2);
	if (fd == -1) {
		printk("Failed to open socket!\n");
		goto clean_up;
	}

	/* Setup TLS socket options */
	err = tls_setup(fd);
	if (err) {
		goto clean_up;
	}

	printk("Connecting to %s\n", "babysense.io");
	err = connect(fd, res->ai_addr, sizeof(struct sockaddr_in));
	if (err) {
		printk("connect() failed, err: %d\n", errno);
		goto clean_up;
	}

        if (fd >= 0) {
		struct http_request req;

		memset(&req, 0, sizeof(req));

		req.method = HTTP_GET;
		req.url = "/weatherforecast";
		req.host = hostString;
		req.protocol = "HTTP/1.1";
		req.response = response_cb;
		req.recv_buf = recv_buf_ipv4;
		req.recv_buf_len = sizeof(recv_buf_ipv4);

		err = http_client_req(fd, &req, timeout, "IPv4 GET");


	}

	recv_buf_ipv4[MAX_RECV_BUF_LEN-1] = '\0';
	printk("%s\n\n", recv_buf_ipv4);


	printk("Finished, closing socket.\n");
        close(fd);

clean_up:
	freeaddrinfo(res);
	(void)close(fd);
}
